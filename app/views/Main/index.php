<!--banner-starts-->
<div class="bnr" id="home">
	<div id="top" class="callbacks_container">
		<ul class="rslides" id="slider4">
			<li>
				<img src="/public/images/bnr-1.jpg" alt=""/>
			</li>
			<li>
				<img src="/public/images/bnr-2.jpg" alt=""/>
			</li>
			<li>
				<img src="/public/images/bnr-3.jpg" alt=""/>
			</li>
		</ul>
	</div>
	<div class="clearfix"> </div>
</div>
<!--banner-ends-->
<!--Slider-Starts-Here-->
<script src="/public/js/responsiveslides.min.js"></script>
<script>
	$(function () {
		$("#slider4").responsiveSlides({
			auto: true,
			pager: true,
			nav: true,
			speed: 500,
			namespace: "callbacks",
			before: function () {
				$('.events').append("<li>before event fired.</li>");
			},
			after: function () {
				$('.events').append("<li>after event fired.</li>");
			}
		});

	});
</script>
<!--End-slider-script-->
<!--about-starts-->
<?php if (isset($brands)): ?>
	<div class="about">
		<div class="container">
			<div class="about-top grid-1">
				<?php foreach($brands as $brand): ?>
					<div class="col-md-4 about-left">
						<figure class="effect-bubba">
							<img class="img-responsive" src="/public/images/<?=$brand['img']?>" alt=""/>
							<figcaption>
							<h2><?=$brand['title']?></h2>
								<p><?=$brand['description']?></p>
							</figcaption>
						</figure>
					</div>
				<?php endforeach; ?>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
<?php endif?>
<!--about-end-->
<!--product-starts-->
<div class="product">
	<div class="container">
		<div class="product-top">
			<div class="product-one">
				<?php $count=0?>
				<?php foreach($hits as $key => $hit): ?>
					<?php $count++?>
					<div class="col-md-3 product-left">
						<div class="product-main simpleCart_shelfItem">
							<a href="product/<?=$hit['alias']?>" class="mask"><img class="img-responsive zoom-img" src="/public/images/<?=$hit['img']?>" alt="" /></a>
							<div class="product-bottom">
								<h3><?=$hit['title']?></h3>
								<p><?=$hit['description']?></p>
								<h4><a class="item_add" href="cart/add?id=<?=$hit['id']?>"><i></i></a> <span class="item_price"><?=\ishop\App::$app->getProperty('currency')['symbol_left']?> <?=\Tools::price_recalculation($hit['price'])?>
									<?php if($hit['old_price'] != 0) : ?>
										</span><small><del><?=\Tools::price_recalculation($hit['old_price'])?></del></small>
									<?php endif; ?>
								</h4>
							</div>
							<?php if($hit['old_price'] != 0) : ?>
								<div class="srch">
									<span>-<?=round(100 - ($hit['price']*100)/$hit['old_price'])?>%</span>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<?php if($count == 4) : ?>
						<div class="clearfix"></div>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
<!--product-end-->