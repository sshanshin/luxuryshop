<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="shortcut icon" href="/public/images/icon-watch.png" type="image/x-icon">
	<?=$this->getMeta()?>
	<link rel="stylesheet" href="/public/css/flexslider.css" type="text/css" media="screen" />

	<link href="/public/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/public/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="/public/megamenu/css/style.css">
	<link rel="stylesheet" href="/public/megamenu/css/ionicons.min.css">
	<link href="/public/css/main.css" rel="stylesheet" type="text/css" media="all" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script src="/public/js/jquery-1.11.0.min.js"></script>
	<script src="/public/js/jquery.easydropdown.js"></script>
	<script src="/public/js/simpleCart.min.js"> </script>
	<script src="/public/megamenu/js/megamenu.js"></script>
	<script src="/public/js/imagezoom.js"></script>
	<script defer src="/public/js/jquery.flexslider.js"></script>
	<script src="/public/js/jquery.easydropdown.js"></script>
	<script type="text/javascript">
		$(function() {
			var menu_ul = $('.menu_drop > li > ul'),
				menu_a  = $('.menu_drop > li > a');
			menu_ul.hide();
			menu_a.click(function(e) {
				e.preventDefault();
				if (!$(this).hasClass('active')) {
					menu_a.removeClass('active');
					menu_ul.filter(':visible').slideUp('normal');
					$(this).addClass('active').next().stop(true,true).slideDown('normal');
				} else {
					$(this).removeClass('active');
					$(this).next().stop(true,true).slideUp('normal');
				}
			});
		});
	</script>
	<title></title>
</head>
<body>

<!--top-header-->
<div class="top-header">
	<div class="container">
		<div class="top-header-main">
			<div class="col-md-6 top-header-left">
				<div class="drop">
					<div class="box">
						<select id="currency" tabindex="4" class="dropdown drop">
							<?php new \app\widgets\currency\Currency()?>
						</select>
					</div>
					<div class="box1">
						<select tabindex="4" class="dropdown">
							<option value="" class="label">English :</option>
							<option value="1">English</option>
							<option value="2">French</option>
							<option value="3">German</option>
						</select>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-md-6 top-header-left">
				<div class="cart box_1">
					<a href="checkout.html">
						<div class="total">
							<span><?=\ishop\App::$app->getProperty('currency')['symbol_left']?> .00</span>
						</div>
						<img src="/public/images/cart-1.png" alt="" />
					</a>
					<p><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--top-header-->
<!--start-logo-->
<div class="logo">
	<a href="/"><h1>Luxury Watches</h1></a>
</div>
<!--start-logo-->
<!--bottom-header-->
<div class="header-bottom">
	<div class="container">
		<div class="header">
			<div class="col-md-9 header-left">
				<div class="menu-container">
					<div class="menu">
						<?php new \app\widgets\menu\Menu([
							'data' => 'test',
							'attrs' => [
								'id' => 'menu'
							],
						])?>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-3 header-right">
				<div class="search-bar">
					<input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
					<input type="submit" value="">
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--bottom-header-->

<?php if(isset($content)) :?>
	<?=$content?>
<?php endif; ?>
<!--information-starts-->
<div class="information">
	<div class="container">
		<div class="infor-top">
			<div class="col-md-3 infor-left">
				<h3>Follow Us</h3>
				<ul>
					<li><a href="#"><span class="fb"></span><h6>Facebook</h6></a></li>
					<li><a href="#"><span class="twit"></span><h6>Twitter</h6></a></li>
					<li><a href="#"><span class="google"></span><h6>Google+</h6></a></li>
				</ul>
			</div>
			<div class="col-md-3 infor-left">
				<h3>Information</h3>
				<ul>
					<li><a href="#"><p>Specials</p></a></li>
					<li><a href="#"><p>New Products</p></a></li>
					<li><a href="#"><p>Our Stores</p></a></li>
					<li><a href="contact.html"><p>Contact Us</p></a></li>
					<li><a href="#"><p>Top Sellers</p></a></li>
				</ul>
			</div>
			<div class="col-md-3 infor-left">
				<h3>My Account</h3>
				<ul>
					<li><a href="account.html"><p>My Account</p></a></li>
					<li><a href="#"><p>My Credit slips</p></a></li>
					<li><a href="#"><p>My Merchandise returns</p></a></li>
					<li><a href="#"><p>My Personal info</p></a></li>
					<li><a href="#"><p>My Addresses</p></a></li>
				</ul>
			</div>
			<div class="col-md-3 infor-left">
				<h3>Store Information</h3>
				<h4>The company name,
				<span>Lorem ipsum dolor,</span>Glasglow Dr 40 Fe 72.
				</h4>
				<h5>+955 123 4567</h5>
				<p><a href="mailto:example@email.com">contact@example.com</a></p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--information-end-->
<!--footer-starts-->
<div class="footer">
	<div class="container">
		<div class="footer-top">
			<div class="col-md-6 footer-left">
				<form>
					<input type="text" value="Enter Your Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Email';}">
					<input type="submit" value="Subscribe">
				</form>
			</div>
			<div class="col-md-6 footer-right">
				<p>© 2015 Luxury Watches. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--footer-end-->
<script src="/public/js/main.js"></script>
<script>
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation: "slide",
			controlNav: "thumbnails"
		});
	});
</script>
</body>
</html>