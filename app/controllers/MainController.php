<?php

namespace app\controllers;
use app\models\AppModel;

class MainController extends AppController {

	public function __construct($route) {
		$this->setMeta('Luxury Watches', 'Luxury Watches A Ecommerce Category Flat Bootstrap Resposive Website Template', 'Luxury Watches Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
			Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design');
		parent::__construct($route);
	}

	public function indexAction() {
		$brands = (new AppModel())->queryS('SELECT * FROM brand LIMIT 3');
		$hits = (new AppModel())->queryS("SELECT * FROM products WHERE hit = '1' AND status = '1' LIMIT 8");
		$this->set(compact( 'brands', 'hits'));
	}
}