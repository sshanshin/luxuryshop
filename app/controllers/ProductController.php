<?php

namespace app\controllers;

use app\models\AppModel;
use app\models\ProductModel;

class ProductController extends AppController {
	public function viewAction() {
		$alias = $this->route['alias'];
		$product = \R::findOne('products','alias = ? AND status = \'1\'', [$alias]);
		if ($product) {
			
			// связанные товары
			$relatedProducts = (new AppModel())->queryS('
				SELECT * FROM products as p
				LEFT JOIN related_products as rp
				ON p.id = rp.related_id
				WHERE rp.product_id='.$product['id'].' LIMIT 3
			');
			
			// галерея продукта
			$galleryProducts = (new AppModel())->queryS('
				SELECT img FROM gallery
				WHERE product_id='.$product['id']);
			
			
			//запись в куки
			$p_model = new ProductModel();
			$p_model->setRecentlyViewed($product['id']);
			
			$r_viewed = $p_model->getRecentlyViewed();
			$viewedProducts = null;
			if ($r_viewed) {
				$viewedProducts = (new AppModel())->queryS('
					SELECT * FROM products
         			WHERE id IN('.implode(',', $r_viewed).')
         		');
			}
			
			$this->set(compact('product', 'relatedProducts', 'galleryProducts', 'viewedProducts'));
			$this->setMeta();
		} else {
			$this->view = 'notFound';
		}
	}
}