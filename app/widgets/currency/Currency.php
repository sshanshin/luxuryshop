<?php

namespace app\widgets\currency;

use app\models\AppModel;
use ishop\App;

class Currency {

	protected $tpl;
	protected $currencies;
	protected $currency;

	public function __construct() {
		$this->tpl = __DIR__.'/currency_tpl/currency.php';
		$this->run();
	}


	protected function run() {
		$this->updateCurrentRate();
		$this->currencies = App::$app->getProperty('currencies');
		$this->currency = App::$app->getProperty('currency');
		echo $this->getHtml();
	}
	public static function getCurrencies() {
		$currencies = (new AppModel())->queryS('SELECT code,title,symbol_left, symbol_right, value, base, date_update FROM currency ORDER BY base DESC');
		$currencyArray = [];
		foreach ($currencies as $currency) {
			$code = $currency['code'];
			unset($currency['code']);
			
			$currencyArray[$code] = $currency;
		}
		return $currencyArray;
//		return \R::getAssoc('SELECT code,title,symbol_left, symbol_right, value, base, date_update FROM currency ORDER BY base DESC');
	}

	public static function getCurrency($currencies) {
		if (isset($_COOKIE['currency']) && array_key_exists(($_COOKIE)['currency'], $currencies)) {
			$key = $_COOKIE['currency'];
		} else {
			$key = key($currencies);
		}
		$currency = $currencies[$key];
		$currency['code'] = $key;
		return $currency;
	}

	public function getHtml() {
		ob_start();
		require_once $this->tpl;
		return ob_get_clean();
	}


	private function updateCurrentRate() {
		$siteAvailible =$this->check_http_status('https://www.cbr-xml-daily.ru/daily_json.jsgetCurrency');
		if ($siteAvailible) {
			$data = json_decode(file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js'));
			$currencies = App::$app->getProperty('currencies') ;
			if ($currencies[СURRENCY]['date_update'] != date('Y-m-d')) {
				foreach ($currencies as $code => $currency) {
					if (isset($data->Valute->$code)) {
						\R::exec( 'UPDATE currency SET value = '.$data->Valute->$code->Value.', date_update = \''.date('Y-m-d').'\' WHERE code = \''.$code.'\'' );
					}
					\R::exec( 'UPDATE currency SET date_update = \''.date('Y-m-d').'\' WHERE code = \'RUB\'' );
				}
			}
		}
 	}

		private function check_http_status($url) {
		return false;
		$headers = get_headers($url);
		$http_code = substr($headers[0], 9, 3);
		if ($http_code == 200) {
			return true;
		}
		return false;
	}

}