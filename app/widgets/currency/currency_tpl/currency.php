<option value="<?=$this->currency['code']?>" class="label"><?=$this->currency['code']?></option>
<?php foreach ($this->currencies as $key => $currency): ?>
	<?php if($key != $this->currency['code']): ?>
		<option value="<?=$key?>"><?=$currency['title']?></option>
	<?php endif;?>
<?php endforeach; ?>