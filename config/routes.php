<?php

use ishop\Router;

Router::add('^product/(?P<alias>[A-Za-z0-9-]+)/?$', ['controller' => 'Product', 'action' => 'view']);

//default routes
Router::add('^admin$', ['controller' => 'Main', 'action' => 'index', 'prefix' => 'admin']);
Router::add('^admin/?(?<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$', ['prefix' => 'admin']);

Router::add('^$', ['controller' => 'Main', 'action' => 'index']);
Router::add('^(?<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');