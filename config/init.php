<?php

define("DEBUG", 1);
define("ROOT", dirname(__DIR__));
define("WWW", ROOT . '/public');
define("APP", ROOT . '/app');
define("CORE", ROOT . '/vendor/ishop/core');
define("LIBS", ROOT . '/vendor/ishop/core/libs');
define("CACHE", ROOT . '/tmp/cache');
define("CACHED", 0);
define("CONFIG", ROOT . '/config');
define("LAYOUT", 'watches');
define('SMARTY', ROOT.'/vendor/smarty/smarty');
define('СURRENCY', 'RUB');


//СОЗДАТЬ ВЫБОР http and https
$protocol = 'http';
$app_path = $protocol.'://'.$_SERVER['HTTP_HOST']; //http://mainshop.lo
define("PATH", $app_path);
define("ADMIN", PATH . '/admin');

require_once ROOT . '/vendor/autoload.php';