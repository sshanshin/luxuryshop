<?php

class Tools {
	public static function getValue($value) {
		$variable = null;
		if (isset($_GET[$value])) {
			$variable = $_GET[$value];
		}
		
		if (!$variable && isset($_POST[$value])) {
			$variable = $_POST[$value];
		}
		return $variable;
	}
	
	public static function redirect($http = false) {
		if ($http) {
			$redirect = $http;
		} else {
			$redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : PATH;
		}
		header("Location: $redirect");
		exit;
	}
	
	public static function price_recalculation($price) {
		$currency = \ishop\App::$app->getProperty('currency');
		return round($price / $currency['value'], 2);
	}
	
	public static function debug($arr) {
		echo '<pre>' .print_r($arr, true). '</pre>';
	}
}