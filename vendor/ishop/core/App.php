<?php

namespace ishop;

class App {
	public static $app;

	public function __construct() {
		$query = $this->filterQuery();
		session_start();
		self::$app = Registry::instance();
		$this->getParams();
		new \ishop\ErrorHandler();
		Router::dispatch($query);
	}

	protected function getParams() {
		$params =  require_once CONFIG . '/params.php';
		if (!empty($params)) {
			foreach ($params as $key => $param) {
				self::$app->setProperty($key, $param);
			}
		}
	}

	private function filterQuery() {
		$url = trim($_SERVER['QUERY_STRING'], '/');
		if (strpos($url, "currency/change") !== false && \Tools::getValue('curr')) {
			return  'currency/change';
		}
		return  $url;
	}
}