<?php

namespace ishop;


class Db {
	use TSingleton;
	protected $pdo;

    protected function __construct() {
        class_alias('\RedBeanPHP\R','\R');
        $db = require_once CONFIG.'/config_db.php';
		$options = [
			\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
			\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
		];
		$this->pdo = new \PDO($db['dsn'], $db['user'], $db['pass'], $options);
		
//		connect R
        \R::setup($db['dsn'], $db['user'], $db['pass']);
        if (!\R::testConnection()) {
            throw new \Exception('Нет соединения с БД!',500);
        }
        \R::freeze(true);
        if (DEBUG) {
//            \R::debug(true);
        }
    }
	
	public function execute($sql) {
		$stmt = $this->pdo->prepare($sql);
		return $stmt->execute();
	}
	
	public function query($sql) {
		$stmt = $this->pdo->prepare($sql);
		$res =  $stmt->execute();
		if ($res !== false) {
			return $stmt->fetchAll();
		}
		return [];
	}
}