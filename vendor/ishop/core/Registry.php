<?php

namespace ishop;

class Registry {
	
	use TSingleton;

	public static $properties = [];

	public static function setProperty($name, $value) {
		self::$properties[$name] = $value;
	}
	
	public static function getProperty($name) {
		return self::$properties[$name] ?? null;
	}
	
	public function getProperties() {
		return self::$properties;
	}
}