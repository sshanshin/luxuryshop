<?php

namespace ishop\base;

use ishop\Db;

abstract class  Model {

	protected $pdo;
	protected $table;

	public $attributes = [];
	public $errors = [];
	public $rules = [];

	public function __construct() {
		$this->pdo = Db::instance();
	}

	public function query($sql) {
		return $this->pdo->execute($sql);
	}

	public function queryS($sql){
		return $this->pdo->query($sql);
	}

	public function findAll($table) {
		$sql = 'SELECT * FROM ' . $table;
		return $this->queryS($sql);
	}
}